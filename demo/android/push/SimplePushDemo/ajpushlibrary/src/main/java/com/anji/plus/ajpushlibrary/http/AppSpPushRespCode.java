package com.anji.plus.ajpushlibrary.http;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 请求返回状态码，0000为正常
 * </p>
 */
public class AppSpPushRespCode {
    public static final String REQUEST_EXCEPTION = "1001";//请求异常
}
