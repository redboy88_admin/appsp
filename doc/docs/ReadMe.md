# 介绍
<h1 align="center">AppSp</h1>

<div align="center">
<p align="center">一站式移动应用服务平台</p>

[![version](https://gitee.com/anji-plus/appsp/badge/star.svg?theme=dark)](https://gitee.com/anji-plus/appsp/stargazers)
![version](./assets/appversion.svg)

</div>

### 在线体验
- admin/ajplus

1. 演示地址: 暂时下线！！
2. 文档地址: [https://ajappsp.beliefteam.cn/appsp-doc](https://ajappsp.beliefteam.cn/appsp-doc)

<div class="custom-block tip"> <p>我们专注做App公共服务，如常用的版本管理、公告管理、推送管理（进行中），在移动领域为中小公司以及个人赋能，减少重复造轮子的成本。AppSp完全开源，有详尽的开发和操作文档，我们将不遗余力，丰富AppSp功能，期待大家的使用并提出宝贵意见。</p></div>



* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis、mybatis-plus。
* 权限认证使用Jwt，支持多终端认证系统。
* APP SDK 原生（iOS、Android） 采用java原生接口调用及系统信息收集
* Flutter SDK 基于APP SDK 基础进行封装，做到一套代码多端复用


### 系统需求
* JDK >= 1.8
* MySQL >= 5.5
* Maven >= 3.0
* iOS >= 9.0
* Vue >= 2.x
* Flutter >= 1.12.0

### 联系我们

