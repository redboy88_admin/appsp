package com.anji.sp.push.service;


import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.po.PushUserPO;
import com.anji.sp.push.model.vo.PushUserVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;


/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushUserService extends IService<PushUserPO> {
    /**
     * 初始化用户数据
     *
     * @param vo
     * @return
     */
    ResponseModel initUserInfo(PushUserVO vo);

    /**
     * 创建
     *
     * @param vo
     * @return
     */
    ResponseModel create(PushUserVO vo);

    /**
     * 根据id修改
     *
     * @param vo
     * @return
     */
    ResponseModel updateByDeviceId(PushUserVO vo);

    /**
     * 根据id删除
     *
     * @param vo
     * @return
     */
    ResponseModel deleteByDeviceId(PushUserVO vo);

    /**
     * 根据id查询一条记录
     *
     * @param vo
     * @return
     */
    ResponseModel queryByDeviceId(PushUserVO vo);


    /**
     * 根据MaunToken查询一条记录
     *
     * @param vo
     * @return
     */
    ResponseModel queryByMaunToken(PushUserVO vo);
    /**
     * 根据ids查询多条记录
     *
     * @param deviceIds
     * @return
     */
    ResponseModel queryByDeviceIds(ArrayList<String> deviceIds,String appKey);

    /**
     * 根据参数分页查询列表
     *
     * @param vo
     * @return
     */
    ResponseModel queryByPage(PushUserVO vo);

    /**
     * 根据deviceType查询列表
     *
     * @param deviceType
     * @return
     */
    ResponseModel queryByDeviceType(String deviceType,String appKey);

    /**
     * 查询所有用户列表
     *
     * @return
     */
    ResponseModel queryAll(String appKey);

    /**
     * 根据查询其他手机品牌或者token为空的安卓用户列表
     *
     * @return
     */
    ResponseModel queryOther(String appKey);
}
