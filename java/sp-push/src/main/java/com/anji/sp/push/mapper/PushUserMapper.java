package com.anji.sp.push.mapper;

import com.anji.sp.push.model.po.PushUserPO;
import com.anji.sp.push.model.vo.PushUserVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushUserMapper extends BaseMapper<PushUserPO> {

    /** XML 自定义分页
     * @param page
     * @param pushUserVO
     * @return
     */
    IPage<PushUserVO> queryByPage(Page<?> page, @Param("pushUserVO") PushUserVO pushUserVO);
}
